#include "LoggerFactory.h"
#include <iostream>

LoggerFactory makeLoggerFactory(std::string prefix)
{
    return [prefix](const LoggerSettings& settings) {
        return Logger([prefix = prefix + " " + settings.prefix](std::string message) {
            std::cout << prefix << ": " << message << std::endl;
        });
    };
}
