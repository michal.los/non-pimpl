#pragma once
#include "LoggerSettings.h"
#include <functional>
#include <string>

template<typename Return, typename... Parameters>
using Function = std::function<Return(Parameters...)>;

template<typename... ToConsume>
using Consumer = Function<void, ToConsume...>;

using Logger = Consumer<std::string>;
using LoggerFactory = Function<Logger, const LoggerSettings&>;

LoggerFactory makeLoggerFactory(std::string prefix);
