#include "LoggerFactory.h"
#include "LoggerSettings.h"

void foo(LoggerFactory& factory)
{
    auto logger = factory(LoggerSettings{"foo"});
    logger("This is foo called");
}

int main()
{
    LoggerFactory factory = makeLoggerFactory("ROOT");
    auto logger = factory(LoggerSettings{"main"});

    logger("Hello World!");

    foo(factory);

    return 0;
}
